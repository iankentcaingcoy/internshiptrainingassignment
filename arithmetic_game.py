from operator import attrgetter
from os.path import isfile
from random import randint
from sys import maxsize
from time import time


FILE_NAME = "userfile"
line_separator = '-'*70
line_gap = '-'*70
user_list = []


class User(object):
    """
    Used to create a single user instance
    """

    def __init__(self, username='', time=maxsize, errors=0):
        """
        User instance constructor

        Args:
            username: (string) username attribute of the user
            time: (string) duration of user's play, default value is 0
            errors: (int) number of wrong answers made by user, default
                value is 0
        """

        self.username = username
        self.time = time
        self.errors = errors

    def is_equal(self, username):
        """
        Checks if username is equivalent to this instance's username

        Args:
            username: (string) value to be compared

        Returns:
            True if this instance's username is equal to the username
            parameter. False otherwise.
        """

        return self.username == username


def read_file_list(filename):
    """
    Function for reading the content of a file and returns a list of
    Users

    Args:
        filename: (string) the name of the container file

    Returns:
        user_list: (list(User)) sequence of User instances from the
            content of the container file
    """

    if not isfile("./{}.txt".format(filename)):
        file_handler = open("{}.txt".format(filename), 'w')
        file_handler.close()
        return []  # returns an empty list

    else:
        user_list = []

        file_handler = open("{}.txt".format(filename), 'r')
        for line in file_handler.xreadlines():
            data = line.strip('\n').split(" :: ")
            user_list.append(User(data[0], int(data[1]), int(data[2])))
        file_handler.close()

        return user_list


def write_file_list(filename, user_list):
    """
    Function for writing the content of user_list into the filename
    file

    Args:
        filename: (string) the name of the container file
        user_list: (list(User)) sequence of User instances
    """

    file_handler = open("{}.txt".format(filename), 'w')
    for user in user_list:
        file_handler.write("{} :: {} :: {}\n".format(user.username, user.time, user.errors))
    file_handler.close()


def print_top_users(user_list):
    """
    Function for printing top 10 users

    Args:
        user_list: (list(User)) sequence of User instances
    """

    print "\n{gap}\n{mid_spanner}TOP 10{mid_spanner}\n{gap}""".format(
        mid_spanner='-'*32,
        gap=line_gap)

    counter = 1
    print "Rank %25s %10s %s" % ("Username", "Time", "Errors")
    for user in sorted(user_list, key=attrgetter('time','errors')):
        if counter > 10:
            break
        minute, second = divmod(user.time, 60)
        print "%-4d %25s %10s %s"%(counter, user.username, "%d:%02d"%(minute, second), user.errors)
        counter += 1


def handle_problem(identifier):
    """
    Function for creating and showing every arithmetic problem

    Args:
        identifier: (int) a random number from 1-4 that will identify
        what operation to create

    Returns:
        (int) number that represent the correct answer to the problem
    """

    def addition_problem():
        """
        Function for creating and showing an addition problem

        Returns:
            (int) sum of adding first_number and second_number
        """

        print "ADDITION"
        first_number = randint(10, 99)
        second_number = randint(10, 99)

        print_problem(first_number, second_number, '+')
        return first_number + second_number

    def subtraction_problem():
        """
        Function for creating and showing a subtraction problem

        Returns:
            (int) difference of subtracting the second_number from the
            first_number
        """

        print "SUBTRACTION"
        first_number = randint(10, 99)
        second_number = randint(10, 99)

        # switch values if second_number is greater than first_number
        if second_number > first_number:
            temporary_variable = first_number
            first_number = second_number
            second_number = temporary_variable
            del temporary_variable  # delete temporary_variable to conserve memory

        print_problem(first_number, second_number, '-')
        return first_number - second_number

    def multiplication_problem():
        """
        Function for creating and showing a multiplication problem

        Returns:
            (int) product of multiplying the first_number and
            second_number
        """

        print "MULTIPLICATION"
        first_number = randint(10, 99)
        second_number = randint(2, 9)

        print_problem(first_number, second_number, 'x')
        return first_number * second_number

    def division_problem():
        """
        Function for creating and showing a division problem

        Returns:
            (int) quotient of dividing the first_number and
            second_number
        """

        print "DIVISION"
        first_number = randint(100, 999)
        second_number = randint(2, 9)

        # get the largest number divisible by second_number lesser than
        # or equal to first_number
        first_number -= first_number%second_number

        print_problem(first_number, second_number, '/')
        return first_number / second_number

    if identifier == 1:
        return addition_problem()
    elif identifier == 2:
        return subtraction_problem()
    elif identifier == 3:
        return multiplication_problem()
    elif identifier == 4:
        return division_problem()
    else:
        return handle_problem(randint(1, 4))


def print_problem(first_number, second_number, operator):
    """
    Function for printing every problem in a readable way

    Args:
        first_number: (int) first operand
        second_number: (int) second operand
        operator: (string) a single character string that denotes the
            arithmetic operation
    """

    print "{} {} {}".format(first_number, operator, second_number)


def get_user_instance_index(username):
    """
    Function for getting the index of the User with the same username
    value in the user_list

    Args:
        username: (string) a user inputed string

    Returns:
        index: (int) index of the first occurrence of User having the
            same username value. If not on list, returns -1
    """

    index = 0
    for user in user_list:
        if user.is_equal(username):
            return index
        index += 1
    return -1


def ask_for_string(prompt):
    """
    Function for asking for a user input

    Args:
        prompt: (string) prompt message

    Returns:
        string: (string) accepted user input
    """

    try:
        string = raw_input(prompt).strip()
        return string

    except KeyboardInterrupt:
        print "\nWarning: You are trying to input an invalid string!"
        return ask_for_string(prompt)

    except EOFError:
        print "\nWarning: You are trying to terminate the game!"
        return ask_for_string(prompt)


def handle_answer(correct_answer):
    """
    Function for asking user's answer and checking if it is equal to
    correct_answer

    Args:
        correct_answer: (int) correct answer for the current arithmetic
            problem

    Returns:
        True if user input is the correct answer to the problem. False
        otherwise.
    """

    try:
        user_answer = input("= ")

    except (KeyboardInterrupt, EOFError):
        print "\nWarning: You are trying to terminate the game!\nThis is counted as a wrong answer."
        user_answer = -1

    except (ValueError, Exception):
        print "Input not a Number!"
        user_answer = -1

    finally:
        return user_answer == correct_answer


def handle_challenge_success(index, username, time, errors):
    """
    Function for saving and storing user game data

    Args:
        index: (int) index of the current user in the user_list
        username: (string) current user's username
        time: (int) current user's game duration
        errors: (int) current user's number of errors
    """

    if index >= 0:
        current_user = user_list[index]

        if time < current_user.time:
            current_user.time = time
            current_user.errors = errors

    else:
        current_user = User(username, time, errors)
        user_list.append(current_user)


# Game opening messages
print """{separator}

                    WELCOME TO THE ARITHMETIC GAME

Mechanics: The game is very simple, you just have to input the correct
answer of the displayed arithmetic problem. You will receive terminate
points for every answer. You will win the challenge if you answered 10
problems correctly. But, if you make 10 errors, your game will be
over. Have fun!

{separator}
""".format(separator=line_separator)

# Generate a list of users from a repository file specified by FILE_NAME
user_list = read_file_list(FILE_NAME)

username = ask_for_string("Please enter your username: ")
while len(username):

    print "Hello there player {user}".format(user=username)
    ask_for_string("Press 'Enter' to begin the game.\n")

    points = 0
    errors = 0
    time_start = time()

    while points < 10 and errors < 10:

        print "{gap}\n".format(gap=line_gap)

        correct_answer = handle_problem(randint(1, 4))

        # Get user answer and check if user answer is correct
        if handle_answer(correct_answer):
            print "CORRECT!"
            points += 1

        else:
            print "WRONG! Correct answer is {}".format(correct_answer)
            errors += 1

    else:
        # Get the starting time of the game
        game_duration = int(round(time() - time_start))

        print "{gap}\n".format(gap=line_gap)

        if points >= 10:
            print "Congratulations! You won the challenge. Beat your record next time."

            handle_challenge_success(
                get_user_instance_index(username),
                username,
                game_duration,
                errors)

        elif errors >= 10:
            print "Sorry, you didn't won the challenge."

        minute, second = divmod(game_duration, 60)

        # Prints the user game statistics
        print """{username} Game Data
        Game Time: {time}
        Points: {points}
        Errors: {errors}
        """.format(
            username=username,
            time="%d:%02d"%(minute, second),
            points=points,
            errors=errors)

        print "{gap}\n".format(gap=line_gap)

    # Starting point of another game
    username = ask_for_string("Please enter your username: ")

print_top_users(user_list)

# Update repository file for the session's data
write_file_list(FILE_NAME, user_list)